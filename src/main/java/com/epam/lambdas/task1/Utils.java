package com.epam.lambdas.task1;

import java.util.Scanner;

public class Utils {
  public static int scannerInt(){
    return new Scanner(System.in).nextInt();
  }
  public static String scannerString(){
    return new Scanner(System.in).nextLine();
  }
}
