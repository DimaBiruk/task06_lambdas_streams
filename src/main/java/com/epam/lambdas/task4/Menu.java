package com.epam.lambdas.task4;

import com.epam.lambdas.task1.Utils;
import java.util.ArrayList;
import java.util.List;

public class Menu {
  public void startMenu() {
    System.out.println("Task4");
    System.out.println("Enter yo text lines");

    List<String> ourLine = new ArrayList<>();
    String e = Utils.scannerString();

    while (e.length() != 0){
      ourLine.add(e);
      e = Utils.scannerString();
    }
    System.out.println(ourLine);
  }
}
