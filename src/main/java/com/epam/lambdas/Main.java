package com.epam.lambdas;

import com.epam.lambdas.task1.FunctionalInterfaceTask;
import com.epam.lambdas.task1.Utils;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;

public class Main {
  public static void main(String[] args) {
//    System.out.println("Task1");
   // task1();
    System.out.println("Task3");
    task3();
  }

  private static void task1() {
    FunctionalInterfaceTask task =
        (a, b, c) -> {
          int max = Integer.max(a, b);
          max = Integer.max(max, c);
          return max;
        };
    FunctionalInterfaceTask task1 =
        (a, b, c) -> {
          int avg = (a + b + c) / 3;
          return avg;
        };
    int a = Utils.scannerInt();
    int b = Utils.scannerInt();
    int c = Utils.scannerInt();
    System.out.println(task.task1Method(a, b, c));
    System.out.println(task1.task1Method(a, b, c));
  }
  private static void task3 (){
    List<Integer> list= new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      list.add(new Random().nextInt(50)+1);
    }
    System.out.println(list);

    IntSummaryStatistics intSummaryStatistics = list.stream()
        .mapToInt(value -> value)
        .summaryStatistics();

    double avg = intSummaryStatistics.getAverage();
    System.out.println(intSummaryStatistics);
    list.stream().filter(a -> a > avg).forEach(System.out::println);
  }
}
